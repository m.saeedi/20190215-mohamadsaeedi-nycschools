# 20190215-MohamadSaeedi-NYCSchools

Notes:

1 - For this demo project commits are added directly to master branch but in real project workflow needs to be followed such as Gitflow

2 - Unit tests are not added due to time constraints 

3 - MVVM architecture is followed

4 - RxSwift is used to achieve binding between views and view models
