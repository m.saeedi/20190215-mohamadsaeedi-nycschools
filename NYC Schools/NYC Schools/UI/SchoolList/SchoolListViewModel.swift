//
//  SchoolListViewModel.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation
import RxSwift

class SchoolListViewModel: SchoolListViewModelType {
    var numberOfSchools: Int {
        return schools.count
    }

    private let navigator: SchoolListNavigatorType
    private var services: SchoolListServiceHandler

    private var schools: [School]
    private var offset = 0
    private var moreToLoad: Bool

    private enum Constants {
        static let batchSize = 50
    }

    let loading: Variable<Bool>
    var refresh: Variable<Bool>

    init(navigator: SchoolListNavigatorType, services: SchoolListServiceHandler) {
        self.navigator = navigator
        self.services = services
        self.schools = []

        loading = Variable(false)
        refresh = Variable(false)
        moreToLoad = true
    }

    func shouldLoadMore() -> Bool {
        return moreToLoad
    }

    func loadSchools() {
        let loadingObservable = offset == 0 ? loading : nil

        // shcools are loaded in pages (batches), when last shcool is shown on UI then load school is triggered again
        loadSchools(batchSize: Constants.batchSize, offset: offset, loadingStatus: loadingObservable)
    }

    func did(select indexPath: IndexPath) {
        guard indexPath.row < schools.count else { return }

        let school = schools[indexPath.row]
        navigator.showDetails(for: school)
    }

    func cellViewModel(for indexPath: IndexPath) -> SchoolListCellModelType? {
        guard indexPath.row < schools.count else { return nil }

        let school = schools[indexPath.row]
        return SchoolListCellViewModel(name: school.name, city: school.city, phone: school.phone)
    }

    private func loadSchools(batchSize: Int, offset: Int, loadingStatus: Variable<Bool>?) {
        services.retrieveSchools(limit: batchSize, offset: offset) { [weak self] state in
            guard let `self` = self else { return }

            switch state {
            case .success(let schools):
                self.schools.append(contentsOf: schools)
                self.offset = offset + schools.count

                // api response does not give information on remaining schools to load so to detect if there are more schools to load, shool count is checked in current response, if number of schools in response is 0 then it is assumed there are no more schools to load
                self.moreToLoad = schools.count > 0
                self.refresh.value = true
                loadingStatus?.value = false
            case .failure:
                // the associated error could used to show proper message on UI to user
                loadingStatus?.value = false
            case .loading:
                loadingStatus?.value = true
            }
        }
    }
}
