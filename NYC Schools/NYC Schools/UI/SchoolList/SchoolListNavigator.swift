//
//  SchoolListNavigator.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation
import UIKit

protocol NavigatorType {
    func load() -> UIViewController
}

protocol SchoolListNavigatorType: NavigatorType {
    func showDetails(for school: School)
}

class SchoolListNavigator: SchoolListNavigatorType  {

    let navigationController: UINavigationController

    init(navigation: UINavigationController) {
        self.navigationController = navigation
    }

    func load() -> UIViewController {
        let listServices = SchoolListService()

        let viewModel = SchoolListViewModel(navigator: self, services: listServices)
        let viewController = SchoolListViewController(viewModel: viewModel)
        return viewController
    }

    func showDetails(for school: School) {
        let detailsNavigator = SchoolDetailsNavigator(with: school)
        self.navigationController.pushViewController(detailsNavigator.load(), animated: true)
    }
}
