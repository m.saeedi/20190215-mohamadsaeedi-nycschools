//
//  SchoolListCellViewModel.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

struct SchoolListCellViewModel: SchoolListCellModelType {
    var name: String
    var city: String
    var phone: String
}
