//
//  SchoolListCell.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import UIKit

protocol SchoolListCellModelType {
    var name: String { get }
    var city: String { get }
    var phone: String { get }
}

class SchoolListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!

    private var viewModel: SchoolListCellModelType? {
        didSet {
            bind()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(using model: SchoolListCellModelType?) {
        viewModel = model
    }

    private func bind() {
        nameLabel.text = viewModel?.name
        cityLabel.text = viewModel?.city
        phoneLabel.text = viewModel?.phone
    }
}
