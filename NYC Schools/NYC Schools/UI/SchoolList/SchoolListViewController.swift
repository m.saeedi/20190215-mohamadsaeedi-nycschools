//
//  SchoolListViewController.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import UIKit
import RxSwift
import RxOptional

protocol SchoolListViewModelType {
    var numberOfSchools: Int { get }
    var loading: Variable<Bool> { get }
    var refresh: Variable<Bool> { get }

    func loadSchools()
    func shouldLoadMore() -> Bool
    func did(select indexPath: IndexPath)
    func cellViewModel(for indexPath: IndexPath) -> SchoolListCellModelType?
}

class SchoolListViewController: UIViewController {

    @IBOutlet weak var list: UITableView!

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private let viewModel: SchoolListViewModelType
    private let disposeBag = DisposeBag()

    private enum Constants {
        static let title = "Schools"
        static let reuseIdentifier = "SchoolListCell"

        static let cellHeight: CGFloat = 72.0
    }

    init(viewModel: SchoolListViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: "SchoolListViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // this should be localized but due to time constraint just adding the text
        title = Constants.title

        list.dataSource = self
        list.delegate = self

        let nib = UINib(nibName: "SchoolListCell", bundle: nil)
        list.register(nib, forCellReuseIdentifier: Constants.reuseIdentifier)

        bind()
    }

    private func bind() {
        viewModel.loadSchools()

        // as driver converts observable to driver to make sure UI updates are done on main thread

        self.viewModel.loading
            .asDriver()
            .drive(onNext: { [weak self] loading in
                self?.activityIndicator(show: loading)
            })
            .disposed(by: disposeBag)

        self.viewModel.refresh.asDriver()
            .drive(onNext: { [weak self] _ in
                self?.list.reloadData()
                self?.list.tableFooterView = nil
            })
            .disposed(by: disposeBag)
    }

    private func activityIndicator(show: Bool) {
        if show {
            self.activityIndicator.startAnimating()
            UIView.animate(withDuration: 0.2, animations: {
                self.indicatorView.alpha = 1.0
            })
        } else {
            self.activityIndicator.stopAnimating()
            UIView.animate(withDuration: 0.2, animations: {
                self.indicatorView.alpha = 0.0
            })
        }
    }
}

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfSchools
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.reuseIdentifier, for: indexPath)

        if let listCell = cell as? SchoolListCell {
            let cellViewModel = viewModel.cellViewModel(for: indexPath)
            listCell.configure(using: cellViewModel)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.did(select: indexPath)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1

        // show loading indicator as table footer view if last cell is displayed and there are more to load
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && viewModel.shouldLoadMore() {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

            self.list.tableFooterView = spinner
            self.list.tableFooterView?.isHidden = false

            viewModel.loadSchools()
        }
    }
}
