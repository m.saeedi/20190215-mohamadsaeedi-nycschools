//
//  SchoolDetailsNavigator.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 16/02/2019.
//

import Foundation
import UIKit

protocol SchoolDetailsNavigatorType: NavigatorType {

}

class SchoolDetailsNavigator: SchoolDetailsNavigatorType  {

    let school: School

    init(with school: School) {
        self.school = school
    }

    func load() -> UIViewController {
        let satDetailsServices = SatDetailsService()

        let viewModel = SchoolDetailsViewModel(with: school, navigator: self, services: satDetailsServices)
        let viewController = SchoolDetailsViewController(viewModel: viewModel)
        return viewController
    }
}
