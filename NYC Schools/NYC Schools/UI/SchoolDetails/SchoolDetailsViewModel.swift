//
//  SchoolDetailsViewModel.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 16/02/2019.
//

import Foundation
import RxSwift

class SchoolDetailsViewModel: SchoolDetailsViewModelType {

    var schoolName: Variable<String>
    let latitude: Variable<Double>
    let longitude: Variable<Double>
    let mathScore: Variable<String?>
    let readingScore: Variable<String?>
    let writingScore: Variable<String?>
    let numberofTestTakers: Variable<String?>

    private let navigator: SchoolDetailsNavigatorType
    private var services: SatDetailsServiceHandler

    private var school: School

    init(with school: School, navigator: SchoolDetailsNavigatorType, services: SatDetailsServiceHandler) {
        self.navigator = navigator
        self.services = services
        self.school = school

        schoolName = Variable(school.name)

        let locationLatitude = Double(school.latitude) ?? 0.0
        let locationLongitude = Double(school.longitude) ?? 0.0
        latitude = Variable(locationLatitude)
        longitude = Variable(locationLongitude)
        mathScore = Variable("")
        readingScore = Variable("")
        writingScore = Variable("")
        numberofTestTakers = Variable("")

        if let _ = self.school.satDetails {
            self.refreshSatsInfoOnView(for: self.school)
        } else {
            services.retrieveSatDetails(for: school.dbn) { [weak self] loadState in
                guard let `self` = self else { return }

                switch loadState {
                case .success(let sats):
                    self.school.satDetails = sats
                    self.refreshSatsInfoOnView(for: self.school)
                case .failure(let error):
                    // appropriate message could be shown on UI
                    debugPrint("unable to retrieve sats details: \(error)")
                case .loading(let message):
                    // show loading indicator similar to the schools list page while waiting for response
                    debugPrint("loading - \(message)")
                }
            }
        }

        self.refreshSatsInfoOnView(for: school)
    }

    private func refreshSatsInfoOnView(for school: School) {
        self.mathScore.value = school.satDetails?.mathScore
        self.readingScore.value = school.satDetails?.readingScore
        self.writingScore.value = school.satDetails?.writingScore
        self.numberofTestTakers.value = school.satDetails?.numberOfTestTakers
    }
}
