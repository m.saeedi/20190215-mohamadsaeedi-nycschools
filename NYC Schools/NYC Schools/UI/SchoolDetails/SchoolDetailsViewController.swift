//
//  SchoolDetailsViewController.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 16/02/2019.
//

import UIKit
import MapKit
import RxSwift
import RxOptional

protocol SchoolDetailsViewModelType {
    var latitude: Variable<Double> { get }
    var longitude: Variable<Double> { get }
    var schoolName: Variable<String> { get }
    var mathScore: Variable<String?> { get }
    var readingScore: Variable<String?> { get }
    var writingScore: Variable<String?> { get }
    var numberofTestTakers: Variable<String?> { get }
}

class SchoolDetailsViewController: UIViewController {

    private let viewModel: SchoolDetailsViewModelType
    private let disposeBag = DisposeBag()

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var totalTestTakersLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!

    @IBOutlet weak var mapView: MKMapView!

    private enum Constants {
        static let title = "School Details"
    }

    init(viewModel: SchoolDetailsViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: "SchoolDetailsViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // this should be localized but due to time constraint just adding the text
        title = Constants.title

        bind()
        self.mapView.showsUserLocation = true
    }

    private func bind() {

        // asDriver drive, updates on main thread (UI updates are done on main thread)

        viewModel.schoolName.asDriver().drive(nameLabel.rx.text).disposed(by: disposeBag)
        viewModel.mathScore.asDriver().drive(mathScoreLabel.rx.text).disposed(by: disposeBag)
        viewModel.writingScore.asDriver().drive(writingScoreLabel.rx.text).disposed(by: disposeBag)
        viewModel.readingScore.asDriver().drive(readingScoreLabel.rx.text).disposed(by: disposeBag)
        viewModel.numberofTestTakers.asDriver().drive(totalTestTakersLabel.rx.text).disposed(by: disposeBag)

        Observable.combineLatest(viewModel.latitude.asObservable(), viewModel.longitude.asObservable()) { ($0, $1) }
            .asDriver(onErrorJustReturn: (0.0, 0.0))
            .drive(onNext: { [weak self] lat, long in
                let annotation = MKPointAnnotation()
                let centerCoordinate = CLLocationCoordinate2D(latitude: lat, longitude:long)
                annotation.coordinate = centerCoordinate
                self?.mapView.addAnnotation(annotation)

                let region = MKCoordinateRegion(center: centerCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
                self?.mapView.setRegion(region, animated: true)
            })
            .disposed(by: disposeBag)
    }
}
