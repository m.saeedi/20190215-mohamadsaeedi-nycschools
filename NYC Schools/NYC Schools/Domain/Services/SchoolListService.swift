//
//  SchoolListService.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

typealias SchoolListServiceCompletion = (LoadState<[School], AppError>) -> Void

protocol SchoolListServiceHandler {
    func retrieveSchools(limit: Int, offset: Int, state: @escaping SchoolListServiceCompletion)
}

class SchoolListService {

    let connectionHandler: ConnectionHandlerType?

    init(connectionHandler: ConnectionHandlerType = ConnectionHandler()) {
        self.connectionHandler = connectionHandler
    }
}

extension SchoolListService: SchoolListServiceHandler {
    func retrieveSchools(limit: Int, offset: Int, state: @escaping SchoolListServiceCompletion) {
        state(.loading(""))

        connectionHandler?.makeConnection(ServiceEndPoints.schools(limit: limit, offset: offset)) { loadState in
            switch loadState {
            case let .success(content):
                do {
                    let decoder = JSONDecoder()
                    let schools = try decoder.decode([School].self, from: content)
                    state(.success(schools))
                } catch {
                    state(.failure(.parser))
                }
            case let .failure(error):
                state(.failure(error))
            default:
                break
            }
        }
    }
}
