//
//  SatDetailsService.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

typealias SatDetailsServiceCompletion = (LoadState<SatDetails?, AppError>) -> Void

protocol SatDetailsServiceHandler {
    func retrieveSatDetails(for school: String, state: @escaping SatDetailsServiceCompletion)
}

class SatDetailsService {

    let connectionHandler: ConnectionHandlerType?

    init(connectionHandler: ConnectionHandlerType = ConnectionHandler()) {
        self.connectionHandler = connectionHandler
    }
}

extension SatDetailsService: SatDetailsServiceHandler {
    func retrieveSatDetails(for school: String, state: @escaping SatDetailsServiceCompletion) {
        state(.loading(""))

        connectionHandler?.makeConnection(ServiceEndPoints.satData(id: school)) { loadState in
            switch loadState {
            case let .success(content):
                do {
                    let decoder = JSONDecoder()
                    let satDetails = try decoder.decode([SatDetails].self, from: content)
                    state(.success(satDetails.first))
                } catch {
                    state(.failure(.parser))
                }
            case let .failure(error):
                state(.failure(error))
            default:
                break
            }
        }
    }
}
