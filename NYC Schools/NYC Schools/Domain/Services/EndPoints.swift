//
//  EndPoints.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

enum ServiceEndPoints {
    case schools(limit: Int, offset: Int)
    case satData(id: String)
}

extension ServiceEndPoints: ResourceType {
    var path: String {
        switch self {
        case .schools:
            return "/resource/s3k6-pzi2.json"
        case .satData:
            return "/resource/f9bf-2cp4.json"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .schools, .satData:
            return .GET
        }
    }

    var headers: [String: String] {
        switch self {
        case .schools, .satData:
            // apis do work without this token but if there is token it can be provided here
            return ["X-App-Token": ""]
        }
    }

    var parameters: [URLQueryItem]? {
        switch self {
        case let .schools(limit, offset):
            return [URLQueryItem(name: "$limit", value: "\(limit)"),
                    URLQueryItem(name: "$offset", value: "\(offset)"),
                    URLQueryItem(name: "$order", value: "school_name"),
                    URLQueryItem(name: "$select", value: "school_name,phone_number,website,primary_address_line_1,city,school_email,zip,latitude, longitude,attendance_rate,total_students,dbn")]
        case let .satData(schoolId):
            return [URLQueryItem(name: "$where", value: "dbn='\(schoolId)'")]
        }
    }
}
