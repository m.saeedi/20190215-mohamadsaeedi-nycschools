//
//  LoadState.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

enum LoadState<T, Error> {
    case loading(String)
    case success(T)
    case failure(Error)
}

enum AppError: Error {
    case offline
    case network(String)
    case parser
}
