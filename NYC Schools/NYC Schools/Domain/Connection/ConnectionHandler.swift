//
//  ConnectionHandler.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

protocol ConnectionHandlerType {
    func makeConnection(_ resource: ResourceType, completion: @escaping (LoadState<Data, AppError>) -> Void)
}

final class ConnectionHandler {
    fileprivate let session: URLSession
    fileprivate let baseURL: URL

    init(basePath: String = "https://data.cityofnewyork.us", session: URLSession = URLSession(configuration: URLSessionConfiguration.default)) {
        self.baseURL = URL(string: basePath)!
        self.session = session
    }
}

extension ConnectionHandler: ConnectionHandlerType {

    func makeConnection(_ resource: ResourceType, completion: @escaping (LoadState<Data, AppError>) -> Void) {

        guard let request = resource.toRequest(baseURL: baseURL) else {
            return
        }

        session.dataTask(with: request, completionHandler: { (data, response, error) in
            switch (data, error) {
            case(let data?, _): completion(.success(data))
            case(_, _): completion(.failure(.network("network error")))
            }
        }) .resume()
    }
}
