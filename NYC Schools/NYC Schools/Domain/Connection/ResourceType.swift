//
//  ResourceType.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
}

protocol ResourceType {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: [URLQueryItem]? { get }
    var headers: [String: String] { get }

    func toRequest(baseURL: URL) -> URLRequest?
}

extension ResourceType {

    func toRequest(baseURL: URL) -> URLRequest? {
        let urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)

        guard var components = urlComponents else {
            return nil
        }
        components.path = path
        components.queryItems = parameters

        guard let url = components.url  else {
            return nil
        }
        let request = NSMutableURLRequest(url: url)

        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        return request as URLRequest
    }
}
