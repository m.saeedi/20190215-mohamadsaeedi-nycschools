//
//  File.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

struct School {
    let dbn: String
    let name: String
    let email: String
    let primaryAddress: String
    let phone: String
    let website: String
    let zip: String
    let totalStudents: String
    let latitude: String
    let longitude: String
    let city: String
    let attendanceRate: String

    var satDetails: SatDetails?

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case name = "school_name"
        case email = "school_email"
        case primaryAddress = "primary_address_line_1"
        case phone = "phone_number"
        case website = "website"
        case zip = "zip"
        case totalStudents = "total_students"
        case latitude = "latitude"
        case longitude = "longitude"
        case city = "city"
        case attendanceRate = "attendance_rate"
    }
}

extension School: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        dbn = try values.decode(String.self, forKey: .dbn)
        name = (try? values.decode(String.self, forKey: .name)) ?? ""
        email = (try? values.decode(String.self, forKey: .email)) ?? ""
        primaryAddress = (try? values.decode(String.self, forKey: .primaryAddress)) ?? ""
        phone = (try? values.decode(String.self, forKey: .phone)) ?? ""
        website = (try? values.decode(String.self, forKey: .website)) ?? ""
        zip = (try? values.decode(String.self, forKey: .zip)) ?? ""
        totalStudents = (try? values.decode(String.self, forKey: .totalStudents)) ?? ""
        latitude = (try? values.decode(String.self, forKey: .latitude)) ?? ""
        longitude = (try? values.decode(String.self, forKey: .longitude)) ?? ""
        city = (try? values.decode(String.self, forKey: .city)) ?? ""
        attendanceRate = (try? values.decode(String.self, forKey: .attendanceRate)) ?? ""
    }
}
