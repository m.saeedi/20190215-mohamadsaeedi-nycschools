//
//  SatDetails.swift
//  NYC Schools
//
//  Created by Mohamad Saeedi on 15/02/2019.
//

import Foundation

struct SatDetails {
    let dbn: String
    let numberOfTestTakers: String
    let readingScore: String
    let mathScore: String
    let writingScore: String

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}

extension SatDetails: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        dbn = try values.decode(String.self, forKey: .dbn)
        numberOfTestTakers = (try? values.decode(String.self, forKey: .numberOfTestTakers)) ?? ""
        readingScore = (try? values.decode(String.self, forKey: .readingScore)) ?? ""
        mathScore = (try? values.decode(String.self, forKey: .mathScore)) ?? ""
        writingScore = (try? values.decode(String.self, forKey: .writingScore)) ?? ""
    }
}
